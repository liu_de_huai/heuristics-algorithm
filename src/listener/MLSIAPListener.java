package listener;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;

import entity.MLSIAPSetting;

public class MLSIAPListener extends AnalysisEventListener<MLSIAPSetting> {
	public List<MLSIAPSetting> list = new ArrayList<MLSIAPSetting>();

	@Override
	public void doAfterAllAnalysed(AnalysisContext arg0) {
		System.out.println("读取完毕！");
	}

	@Override
	public void invoke(MLSIAPSetting arg0, AnalysisContext arg1) {
		list.add(arg0);
	}

}
