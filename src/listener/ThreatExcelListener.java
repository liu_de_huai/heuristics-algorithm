package listener;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;

import entity.ThreatExcel;

public class ThreatExcelListener extends AnalysisEventListener<ThreatExcel> {

	public List<ThreatExcel> list = new ArrayList<ThreatExcel>();

	@Override
	public void doAfterAllAnalysed(AnalysisContext arg0) {
		System.out.println("读取完毕！");
	}

	@Override
	public void invoke(ThreatExcel arg0, AnalysisContext arg1) {
		list.add(arg0);
	}

}
