package listener;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;

import entity.Ndh;

public class NdhListener extends AnalysisEventListener<Ndh> {
	public List<Ndh> list = new ArrayList<Ndh>();

	@Override
	public void doAfterAllAnalysed(AnalysisContext arg0) {
		System.out.println("读取完毕！");
	}

	@Override
	public void invoke(Ndh arg0, AnalysisContext arg1) {
		list.add(arg0);
	}

}
