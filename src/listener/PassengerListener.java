package listener;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;

import entity.Passenger;

public class PassengerListener extends AnalysisEventListener<Passenger> {

	public List<Passenger> list = new ArrayList<Passenger>();

	@Override
	public void doAfterAllAnalysed(AnalysisContext arg0) {
		System.out.println("读取完毕！");
	}

	@Override
	public void invoke(Passenger arg0, AnalysisContext arg1) {
		list.add(arg0);
	}

}
