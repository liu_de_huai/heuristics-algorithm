package listener;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;

import entity.DeviceTypeExcel;

public class DeviceTypeExcelListener extends AnalysisEventListener<DeviceTypeExcel> {

	public List<DeviceTypeExcel> list = new ArrayList<DeviceTypeExcel>();

	@Override
	public void doAfterAllAnalysed(AnalysisContext arg0) {
		System.out.println("读取完毕！");
	}

	@Override
	public void invoke(DeviceTypeExcel arg0, AnalysisContext arg1) {
		list.add(arg0);
	}

}
