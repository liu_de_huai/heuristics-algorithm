//package algorithm;
//
//import entity.TwoTuple;
//import problem.Problem;
//import solution.Solution;
//
///**
// * 水波优化算法用于求解背包问题
// * 
// * @author Administrator
// *
// */
//public class WWOForKnapsackProblem extends WWOForCombinatorialProblem {
//
//	public WWOForKnapsackProblem(Problem<Integer> problem, boolean isMax, String algName, int popSize,
//			int max_Iterations, int hmax, double alpha, double beta, int kmax, double lambda) {
//		super(problem, isMax, algName, popSize, max_Iterations, hmax, alpha, beta, kmax, lambda);
//	}
//
//	@Override
//	public void propagate() {
//
//	}
//
//	@Override
//	public void refract() {
//		// TODO Auto-generated method stub
//
//	}
//
//	@Override
//	public void breaking() {
//		// TODO Auto-generated method stub
//
//	}
//
//	@Override
//	public TwoTuple<Solution<Integer>, Double> run() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public void updateParameters() {
//		// TODO Auto-generated method stub
//
//	}
//
//	@Override
//	public Solution<Integer> getBestSolution() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public Solution<Integer> getWorstSolution() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public void update() {
//
//	}
//
//	@Override
//	public void evolve() {
//
//	}
//
//	@Override
//	public String toString() {
//		return "WWOForKnapsackProblem [hmax=" + hmax + ", alpha=" + alpha + ", beta=" + beta + ", kmax=" + kmax
//				+ ", lambda=" + lambda + ", problem=" + problem + ", random=" + random + ", isMax=" + isMax
//				+ ", algName=" + algName + ", best=" + best + ", population=" + population + ", popSize=" + popSize
//				+ ", max_Iterations=" + max_Iterations + ", timeTaken=" + timeTaken + ", run()=" + run()
//				+ ", getBestSolution()=" + getBestSolution() + ", getWorstSolution()=" + getWorstSolution()
//				+ ", getHmax()=" + getHmax() + ", getAlpha()=" + getAlpha() + ", getBeta()=" + getBeta()
//				+ ", getKmax()=" + getKmax() + ", getLambda()=" + getLambda() + ", getProblem()=" + getProblem()
//				+ ", getRandom()=" + getRandom() + ", getBest()=" + getBest() + ", getPopulation()=" + getPopulation()
//				+ ", getPopSize()=" + getPopSize() + ", getTimeTaken()=" + getTimeTaken() + ", isMax()=" + isMax()
//				+ ", getAlgName()=" + getAlgName() + ", getMax_Iterations()=" + getMax_Iterations() + ", getClass()="
//				+ getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
//	}
//
//	@Override
//	protected void checkBound(Solution<Integer> solution, int index) {
//		
//	}
//
//}
