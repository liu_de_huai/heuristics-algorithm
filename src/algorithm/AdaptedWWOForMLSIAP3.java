package algorithm;

import entity.FourTuple;
import problem.Problem;
import solution.WaveForMLSIAP;
/**
 * 在改进的breaking操作上，删除refraction之后的波长更新公式
 * @author Dehuai Liu
 *
 */
public class AdaptedWWOForMLSIAP3 extends AdaptedWWOForMLSIAP1 {

	public AdaptedWWOForMLSIAP3(Problem<Integer, FourTuple<Double, Double, Integer, Double>, Double> problem,
			boolean isMax, String algName, int popSize, int NFE, int hmax, double alpha, double betaMax, double betaMin,
			int kmax, double lambdaMax, double lambdaMin, double upsilon) {
		super(problem, isMax, algName, popSize, NFE, hmax, alpha, betaMax, betaMin, kmax, lambdaMax, lambdaMin,
				upsilon);
	}
	@Override
	public WaveForMLSIAP refract(WaveForMLSIAP wave) {
		WaveForMLSIAP newWave = null;
		try {
			newWave = (WaveForMLSIAP) wave.clone();
			Integer[] content = newWave.getContent();
			Integer[] bestContent = best.getContent();
			for (int d = 0; d < N; d++) {
				if (random.nextDouble() < upsilon) {
					content[d] = bestContent[d];
				}
			}
			// 分配检查人员
			assignInspectors(content);
			// 重新评估
			evaluateOneWave(newWave);

		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return newWave;
	}

}
