package algorithm;

import entity.FourTuple;
import problem.Problem;

public class AdaptedSimWWOForMLSIAP extends SimpleWWOAlgForMLSIAP {

	public AdaptedSimWWOForMLSIAP(Problem<Integer, FourTuple<Double, Double, Integer, Double>, Double> problem,
			boolean isMax, String algName, int NFE, double alpha, double betaMax, double betaMin, int kmax,
			double lambda, int popSizeMax, int popSizeMin) {
		super(problem, isMax, algName, NFE, alpha, betaMax, betaMin, kmax, lambda, popSizeMax, popSizeMin);
	}

}
