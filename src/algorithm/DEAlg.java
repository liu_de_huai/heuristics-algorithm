package algorithm;

import problem.Problem;
import solution.Solution;

public abstract class DEAlg<T1 extends Number, T2 extends Number, S extends Solution<T1, T2>, Tuple>
		extends Algorithm<T1, T2, S, Tuple> {

	/* DE特有的属性 */
	double F;// step size
	double CR;// 交叉概率

	public DEAlg(Problem<T1, Tuple, T2> problem, boolean isMax, String algName, int popSize, int NFE, double F, double CR) {
		super(problem, isMax, algName, popSize, NFE);
		this.F = F;
		this.CR = CR;
	}

	/* DE 特有的方法 */
	/**
	 * 变异 随机从种群中选取三个向量，完成变异
	 * 
	 * @return
	 */
	public abstract S mutate(S target);

	/**
	 * 交叉
	 * 
	 * @param mutatedVector
	 * @param originVector
	 * @return
	 */
	public abstract S cross(S originVector, S mutatedVector);

	/**
	 * 选择
	 * 
	 * @param originVector
	 * @param trialVector
	 * @return
	 */
	public abstract S select(S originVector, S trialVector);

}
