package algorithm;

import entity.FourTuple;
import problem.Problem;

public class AdaptedWWOForMLSIAP4 extends AdaptedWWOForMLSIAP {

	public AdaptedWWOForMLSIAP4(Problem<Integer, FourTuple<Double, Double, Integer, Double>, Double> problem,
			boolean isMax, String algName, int popSize, int NFE, int hmax, double alpha, double betaMax, double betaMin,
			int kmax, double lambdaMax, double lambdaMin, double upsilon) {
		super(problem, isMax, algName, popSize, NFE, hmax, alpha, betaMax, betaMin, kmax, lambdaMax, lambdaMin,
				upsilon);
	}

}
