//package algorithm;
//
//import problem.Problem;
//import solution.Solution;
//
///**
// * 水波优化-组合优化问题
// * 
// * @author Administrator
// *
// */
//public abstract class WWOForCombinatorialProblem extends WWOAlgorithm<Integer, Double, Solution<Integer,Double>> {
//
//
//	public WWOForCombinatorialProblem(Problem<Integer, Solution<Integer, Double>, Double> problem, boolean isMax,
//			String algName, int popSize, int max_Iterations, int hmax, double alpha, double beta, int kmax,
//			double lambda) {
//		super(problem, isMax, algName, popSize, max_Iterations, hmax, alpha, beta, kmax, lambda);
//	}
//
//	@Override
//	public void initialize() {
//		int n = problem.getDimension();
//		Integer upper = problem.getUpper();
//		Integer lower = problem.getLower();
//		for (int i = 0; i < popSize; i++) {
//			Integer[] content = new Integer[n];
//			for (int j = 0; j < content.length; j++) {
//				content[j] = random.nextInt(upper - lower + 1) + lower;
//			}
//			WWOForKnapsackSolution solution = new WWOForKnapsackSolution(content, upper, lower, 0, i, n, hmax, lambda);
//			population.add(solution);
//		}
//
//	}
//}
