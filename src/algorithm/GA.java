package algorithm;

import problem.Problem;
import solution.Solution;

public abstract class GA<T1 extends Number, T2 extends Number, S extends Solution<T1, T2>, Tuple>
		extends Algorithm<T1, T2, S, Tuple> {
	/* GA特有的属性 */
	double Pc;// 交叉概率
	double Pm;// 变异概率

	public GA(Problem<T1, Tuple, T2> problem, boolean isMax, String algName, int popSize, int NFE, double pc,
			double pm) {
		super(problem, isMax, algName, popSize, NFE);
		Pc = pc;
		Pm = pm;
	}

	/* GA特有的方法 */
	/**
	 * 选择
	 * 
	 * @return
	 */
	public abstract void select();

	/**
	 * 交配
	 */
	public abstract void breeding();

	/**
	 * 变异
	 */
	public abstract void mutation();

	public double getPc() {
		return Pc;
	}

	public void setPc(double pc) {
		Pc = pc;
	}

	public double getPm() {
		return Pm;
	}

	public void setPm(double pm) {
		Pm = pm;
	}

}
