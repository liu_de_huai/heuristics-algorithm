//package algorithm;
//
//import problem.Problem;
//import solution.Solution;
//import solution.WaveForContinuousProblem1;
//
///**
// * 水波优化算法-连续优化问题
// * 
// * @author Administrator
// *
// */
//public abstract class WWOAlgForContinuousProblem extends WWOAlgorithm<Double, Solution<Double>> {
//
//	public WWOAlgForContinuousProblem(Problem<Double> problem, boolean isMax, String algName, int popSize,
//			int max_Iterations, int hmax, double alpha, double beta, int kmax, double lambda) {
//		super(problem, isMax, algName, popSize, max_Iterations, hmax, alpha, beta, kmax, lambda);
//	}
//
//	/**
//	 * 初始化一组连续优化的解
//	 */
//	@Override
//	public void initialize() {
//		int dimension = problem.getDimension();
//		double upper = problem.getUpper();
//		double lower = problem.getLower();
//		for (int i = 0; i < popSize; i++) {
//			Double[] content = new Double[dimension];
//			for (int j = 0; j < content.length; j++) {
//				content[j] = random.nextDouble() * (upper - lower) + lower;
//			}
//			// 用于无法多重继承，这里只能先赋值给一个继承自Wave的连续优化问题解
//			WaveForContinuousProblem1 solution = new WaveForContinuousProblem1(content, upper, lower, 0.0, i, dimension,
//					hmax, lambda);
//			population.add(solution);
//		}
//
//	}
//
//}
