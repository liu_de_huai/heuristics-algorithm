package algorithm;

import problem.Problem;
import solution.Solution;

/**
 * 水波优化算法的父类算法 主要包括所有水波算法都会用到的参数和方法
 * 
 * @author Administrator
 *
 */
public abstract class WWOAlgorithm<T extends Number, T2 extends Number, S extends Solution<T, T2>, Tuple>
		extends Algorithm<T, T2, S, Tuple> {
	/* 水波算法独有的参数 */
	int hmax;// 波的最大高度，也是初始高度
	double alpha;// 波长衰减系数
	double beta;// 碎浪系数
	double betaMax;
	double betaMin;
	int kmax;// 用于碎浪过程中，选择的维度数量
	double lambda;// 为每个波设置的初始波长
	double epsilon = 0.000000001;// 防止0除

	public WWOAlgorithm(Problem<T, Tuple, T2> problem, boolean isMax, String algName, int popSize, int NFE, int hmax,
			double alpha, double betaMax, double betaMin, int kmax, double lambda) {
		super(problem, isMax, algName, popSize, NFE);
		this.hmax = hmax;
		this.alpha = alpha;
		this.betaMax = betaMax;
		this.betaMin = betaMin;
		// beta初始化为betaMax
		this.beta = betaMax;
		this.kmax = kmax;
		this.lambda = lambda;
	}

	public int getHmax() {
		return hmax;
	}

	public void setHmax(int hmax) {
		this.hmax = hmax;
	}

	public double getAlpha() {
		return alpha;
	}

	public void setAlpha(double alpha) {
		this.alpha = alpha;
	}

	public double getBeta() {
		return beta;
	}

	public void setBeta(double beta) {
		this.beta = beta;
	}

	public int getKmax() {
		return kmax;
	}

	public void setKmax(int kmax) {
		this.kmax = kmax;
	}

	public double getLambda() {
		return lambda;
	}

	public void setLambda(double lambda) {
		this.lambda = lambda;
	}

	/** 水波算法独有的操作 **/
	/**
	 * 传播操作
	 */
	public abstract S propagate(S wave);

	/**
	 * 折射操作
	 */
	public abstract S refract(S wave);

	/**
	 * 碎浪操作
	 */
	public abstract S breaking(S wave);

	/**
	 * 更新参数
	 */
	@Override
	public void updateParameters() {
		beta = betaMax - (betaMax - betaMin) * (current_NFE * 1.0 / NFE);
	}
}
