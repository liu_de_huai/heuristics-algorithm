package algorithm;

import problem.Problem;
import solution.Solution;

public abstract class PSOAlg<T extends Number, T2 extends Number, S extends Solution<T, T2>, Tuple>
		extends Algorithm<T, T2, S, Tuple> {
	double w;// inertia weight
	double c1, c2;// learning factor
	T Vmax;// upper bound of velocity

	public PSOAlg(Problem<T, Tuple, T2> problem, boolean isMax, String algName, int popSize, int NFE, double w,
			double c1, double c2, T vmax) {
		super(problem, isMax, algName, popSize, NFE);
		this.w = w;
		this.c1 = c1;
		this.c2 = c2;
		Vmax = vmax;
	}

	public double getW() {
		return w;
	}

	public void setW(double w) {
		this.w = w;
	}

	public double getC1() {
		return c1;
	}

	public void setC1(double c1) {
		this.c1 = c1;
	}

	public double getC2() {
		return c2;
	}

	public void setC2(double c2) {
		this.c2 = c2;
	}

}
