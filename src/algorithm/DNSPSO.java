package algorithm;

import problem.Problem;
import solution.Solution;

/**
 * 带有种群多样性提高机制和邻域搜索的PSO
 * 
 * @author Dehuai Liu
 *
 * @param <T>
 * @param <T2>
 * @param <S>
 * @param <Tuple>
 */
public abstract class DNSPSO<T extends Number, T2 extends Number, S extends Solution<T,T2>, Tuple>
		extends PSOAlg<T, T2, S, Tuple> {

	double pr;// cross probability

	public DNSPSO(Problem<T, Tuple, T2> problem, boolean isMax, String algName, int popSize, int NFE, int w, int c1,
			int c2, T Vmax, double pr) {
		super(problem, isMax, algName, popSize, NFE, w, c1, c2, Vmax);
		this.pr = pr;
	}

}
