package util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.alibaba.excel.EasyExcel;

import entity.DeviceTypeExcel;
import entity.MLSIAPSetting;
import entity.Ndh;
import entity.Passenger;

public class GenerateData {
	static Random random = new Random();

	public static void main(String[] args) {
		// 产生问题实例
		// generateInstances();
		// 产生设备类型
		generateDeveiceTypes();
		// 产生在每一个实例下，每种设备类型的数量
		// generateNumOfDeviceTypes();
		// 产生乘客数据
		// generatePassengers();
		// 产生测试乘客
		// generateTestPassengers(0);
	}

	/**
	 * 产生问题实例
	 */
	public static void generateInstances() {
		List<MLSIAPSetting> list = new ArrayList<MLSIAPSetting>();
		int m1 = 4, m2 = 4, k = 4;
		double omega = 0.5, delta = 0.00007, T = 120.0;
		int NP = 10;
		MLSIAPSetting instance1 = new MLSIAPSetting(1, 20, 2, m1, m2, k, 50, 8, omega, 0.00007, 20000, NP);
		list.add(instance1);
		MLSIAPSetting instance2 = new MLSIAPSetting(2, 80, 5, m1, m2, k, 80, 10, omega, 0.00007, 50000, NP);
		list.add(instance2);
		MLSIAPSetting instance3 = new MLSIAPSetting(3, 160, 20, m1, m2, k, T, 12, omega, 0.000085, 70000, NP);
		list.add(instance3);
		MLSIAPSetting instance4 = new MLSIAPSetting(4, 320, 25, m1, m2, k, T, 18, omega, 0.000085, 70000, NP);
		list.add(instance4);
		MLSIAPSetting instance5 = new MLSIAPSetting(5, 400, 45, m1, m2, k, 125, 22, omega, 0.00009, 100000, NP);
		list.add(instance5);
		MLSIAPSetting instance6 = new MLSIAPSetting(6, 560, 56, m1, m2, k, 125, 25, omega, 0.00009, 100000, 2 * NP);
		list.add(instance6);
		MLSIAPSetting instance7 = new MLSIAPSetting(7, 700, 70, m1, m2, k, 145, 32, omega, 0.00009, 150000, 2 * NP);
		list.add(instance7);
		MLSIAPSetting instance8 = new MLSIAPSetting(8, 1000, 85, m1, m2, k, 145, 38, omega, 0.00009, 200000, 2 * NP);
		list.add(instance8);

		String path = FileUtil.getFilePath() + "\\instance.xlsx";
		EasyExcel.write(path, MLSIAPSetting.class).sheet("问题集").doWrite(list);
	}

	/**
	 * 产生乘客数据
	 */
	@SuppressWarnings("unused")
	private static void generatePassengers() {
		int numOfDevices = 8;
		double piTOfNormalLower = 0.0005;
		double piTOfNormalUpper = 0.001;
		double piTOfDangerLower = 0.05;
		double piTOfDangerUpper = 0.1;
		// 实例1
		int instanceId = 1;
		int N1 = 20;
		int N2 = 2;
		generatePassengerList(instanceId, N1, N2, numOfDevices, piTOfNormalLower, piTOfNormalUpper, piTOfDangerLower,
				piTOfDangerUpper);
		// 实例2
		instanceId = 2;
		N1 = 80;
		N2 = 5;
		generatePassengerList(instanceId, N1, N2, numOfDevices, piTOfNormalLower, piTOfNormalUpper, piTOfDangerLower,
				piTOfDangerUpper);
		// 实例3
		instanceId = 3;
		N1 = 160;
		N2 = 20;
		generatePassengerList(instanceId, N1, N2, numOfDevices, piTOfNormalLower, piTOfNormalUpper, piTOfDangerLower,
				piTOfDangerUpper);
		// 实例4
		instanceId = 4;
		N1 = 320;
		N2 = 25;
		generatePassengerList(instanceId, N1, N2, numOfDevices, piTOfNormalLower, piTOfNormalUpper, piTOfDangerLower,
				piTOfDangerUpper);
		// 实例5
		instanceId = 5;
		N1 = 400;
		N2 = 45;
		generatePassengerList(instanceId, N1, N2, numOfDevices, piTOfNormalLower, piTOfNormalUpper, piTOfDangerLower,
				piTOfDangerUpper);
		// 实例6
		instanceId = 6;
		N1 = 560;
		N2 = 56;
		generatePassengerList(instanceId, N1, N2, numOfDevices, piTOfNormalLower, piTOfNormalUpper, piTOfDangerLower,
				piTOfDangerUpper);
		// 实例7
		instanceId = 7;
		N1 = 700;
		N2 = 70;
		generatePassengerList(instanceId, N1, N2, numOfDevices, piTOfNormalLower, piTOfNormalUpper, piTOfDangerLower,
				piTOfDangerUpper);
		// 实例8
		instanceId = 8;
		N1 = 1000;
		N2 = 85;
		generatePassengerList(instanceId, N1, N2, numOfDevices, piTOfNormalLower, piTOfNormalUpper, piTOfDangerLower,
				piTOfDangerUpper);

	}

	private static void generatePassengerList(int instanceId, int n1, int n2, int numOfDevices, double piTOfNormalLower,
			double piTOfNormalUpper, double piTOfDangerLower, double piTOfDangerUpper) {
		List<Passenger> list = new ArrayList<>();
		int i = 1;
		for (; i <= n1; i++) {
			double piT = (int) ((random.nextDouble() * (piTOfNormalUpper - piTOfNormalLower) + piTOfNormalLower)
					* 1000000) / 1000000.0d;
			Passenger Pi = new Passenger(i, piT, numOfDevices);
			list.add(Pi);
		}
		for (; i <= n1 + n2; i++) {
			double piT = (int) ((random.nextDouble() * (piTOfDangerUpper - piTOfDangerLower) + piTOfDangerLower)
					* 1000000) / 1000000.0d;
			Passenger Pi = new Passenger(i, piT, numOfDevices);
			list.add(Pi);
		}
		String path = FileUtil.getFilePath() + "\\Passenger\\" + instanceId + "passenger.xlsx";
		EasyExcel.write(path, Passenger.class).sheet("乘客实例" + instanceId).doWrite(list);
	}

	private static void generateNumOfDeviceTypes() {
		List<Ndh> list = new ArrayList<>();
		Ndh ndh1 = new Ndh(1, 3, 3, 3, 3, 1, 1, 1, 1);
		list.add(ndh1);
		Ndh ndh2 = new Ndh(2, 3, 3, 3, 3, 2, 2, 2, 2);
		list.add(ndh2);
		Ndh ndh3 = new Ndh(3, 3, 3, 3, 3, 2, 2, 2, 2);
		list.add(ndh3);
		Ndh ndh4 = new Ndh(4, 6, 6, 6, 6, 3, 3, 3, 3);
		list.add(ndh4);
		Ndh ndh5 = new Ndh(5, 6, 6, 6, 6, 3, 3, 3, 3);
		list.add(ndh5);
		Ndh ndh6 = new Ndh(6, 8, 8, 8, 8, 5, 5, 5, 5);
		list.add(ndh6);
		Ndh ndh7 = new Ndh(7, 10, 10, 10, 10, 6, 6, 6, 6);
		list.add(ndh7);
		Ndh ndh8 = new Ndh(8, 12, 12, 12, 12, 7, 7, 7, 7);
		list.add(ndh8);

		String path = FileUtil.getFilePath() + "Device\\NumOfDh.xlsx";
		EasyExcel.write(path, Ndh.class).sheet("设备数量").doWrite(list);
	}

	private static void generateTestPassengers(int instanceNo) {
		List<Passenger> normalPassengers = new ArrayList<>();
		int i = 1;
		int numOfDevices = 8;
		for (; i <= 5; i++) {
			double piTOfNormal = 0.001;
			Passenger normalPass = new Passenger(i, piTOfNormal, numOfDevices);
			normalPassengers.add(normalPass);
		}

		List<Passenger> dangerousPassengers = new ArrayList<>(normalPassengers);
		for (; i <= 10; i++) {
			double pikOfDanger = 0.1;
			Passenger dangerousPass = new Passenger(i, pikOfDanger, numOfDevices);
			dangerousPassengers.add(dangerousPass);
		}
		String path = FileUtil.getFilePath() + "\\Passenger\\" + instanceNo + "passenger.xlsx";
		EasyExcel.write(path, Passenger.class).sheet("实例" + instanceNo).doWrite(dangerousPassengers);
	}

	/**
	 * 详细数据部分已经手动改了，不需要再执行
	 */
	private static void generateDeveiceTypes() {
		List<DeviceTypeExcel> list = new ArrayList<DeviceTypeExcel>();
		// 设置NDh=3,Th=1
		for (int h = 1; h <= 8; h++) {
			DeviceTypeExcel deviceTypeExcel = new DeviceTypeExcel(h, 3, 0.0, 0.0, 0.0, 0.0, 0.98, 1.0);
			list.add(deviceTypeExcel);
		}
		String path = FileUtil.getFilePath() + "\\Device\\4.xlsx";
		EasyExcel.write(path, DeviceTypeExcel.class).sheet("4T8D").doWrite(list);
	}

}
