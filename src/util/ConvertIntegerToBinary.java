package util;

public class ConvertIntegerToBinary {
	/**
	 * 将指定的整数解转化成二进制整数解的形式
	 * 
	 * @param originalSolution
	 * @param length
	 *            每个整数解转化成二进制后的长度
	 * @return
	 */
	public static int[] toBinarySolution(int[] originalSolution, int length) {
		int[] result = new int[originalSolution.length * length];
		for (int i = 0; i < originalSolution.length; i++) {
			String str = Integer.toBinaryString(originalSolution[i]);
			char[] chars = str.toCharArray();
			// 当chars的长度不足length需要在高位补0
			if (chars.length < length) {
				char[] temp = new char[length];
				int j = 0;
				for (; j < length - chars.length; j++) {
					temp[j] = '0';
				}
				for (int k = 0; k < chars.length; k++, j++) {
					temp[j] = chars[k];
				}
				chars = temp;
			}

			for (int j = 0; j < length; j++) {
				result[i * length + j] = chars[j] - '0';
			}
		}
		return result;
	}

	public static void main(String[] args) {
		int[] arr = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
		int length=4;
		int[] result = toBinarySolution(arr, length);
		for (int i = 0; i < result.length; i++) {
			if(i%length==0)
				System.out.print("\r");
			System.out.print(result[i]);
		}
		// int a = 11;
		// System.out.println(Integer.toBinaryString(a));
		// System.out.println(Integer.toBinaryString(11).length());
	}

}
