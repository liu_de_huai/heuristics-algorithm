package util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.excel.EasyExcel;

import entity.FEResult;
import entity.Result;
import entity.TwoTuple;
import solution.SIAPSolution;
import solution.WaveForMLSIAP;

public class FileUtil {
	public static final String MLSIAPPath = "instance.xlsx";
	public static final String EightDeviceFourThreatPath = "Device\\4T8D.xlsx";
	public static final String NumberOfDevice = "Device\\NumOfDh.xlsx";
	public static final String PassengerPrefix = "Passenger\\";
	public static final String Passengersuffix = "passenger.xlsx";
	public static final String FourThreatPath = "Threat\\4threat.xlsx";
	public static final String ResulPrefix = "result\\";
	public static final String ExcelSuffix = ".xlsx";

	public static String getFilePath() {
		return "D:\\工作\\研究生\\论文\\data\\problem\\";
	}

	/**
	 * 存储算法在指定问题集上的数据
	 * 
	 * @param algName
	 * @param InstanceName
	 * @param data
	 */
	public static void storeExperimentData(String algName, String InstanceName,
			List<TwoTuple<SIAPSolution, Double>> data) {
		String direcotry = getFilePath() + ResulPrefix + algName;
		// 创建文件目录
		File file = new File(direcotry);
		file.mkdirs();

		String path = direcotry + "\\" + InstanceName + ExcelSuffix;
		List<Result> list = new ArrayList<Result>();
		Result[] arr = new Result[data.size()];
		// 封装数据
		for (int i = 0; i < data.size(); i++) {
			TwoTuple<SIAPSolution, Double> row = data.get(i);
			Result result = new Result();
			result.setObjectiveValue(row.first.getValue());
			result.setValueOfPFC(row.first.getValueOfPFC());
			result.setMaxTime(row.first.getMaxTime());
			result.setTotalS(row.first.getTotalS());
			result.setRunTime(row.second);
			arr[i] = result;
			// 插入排序
			for (int j = i; j > 0; j--) {
				if (arr[j].getObjectiveValue() < arr[j - 1].getObjectiveValue()) {
					Result temp = arr[j];
					arr[j] = arr[j - 1];
					arr[j - 1] = temp;
				}
			}
		}

		for (int i = 0; i < arr.length; i++) {
			list.add(arr[i]);
		}
		// 存储数据
		EasyExcel.write(path, Result.class).sheet(InstanceName).doWrite(list);
	}

	// /**
	// * 存储算法在指定问题集上每一千次FE的数据
	// *
	// * @param algName
	// * @param instanceName
	// * @param currentIteration
	// * 第i次运行算法
	// * @param data
	// */
	// public static void storeFEData(String algName, String instanceName, int
	// currentIteration,
	// List<TwoTuple<Integer, SIAPSolution>> data) {
	// String directory = FileUtil.getFilePath() + ResulPrefix + algName + "\\" +
	// instanceName + "_NFE";
	// // 创建目录
	// File file = new File(directory);
	// file.mkdirs();
	// // 存储数据
	// List<FEResult> list = new ArrayList<FEResult>();
	// for (int i = 0; i < data.size(); i++) {
	// TwoTuple<Integer, SIAPSolution> tuple = data.get(i);
	// FEResult result = new FEResult();
	// result.setNfe(tuple.first);
	// result.setObjectiveValue(tuple.second.getValue());
	// list.add(result);
	// }
	//
	// String path = directory + "\\" + currentIteration + ExcelSuffix;
	// EasyExcel.write(path, FEResult.class).sheet(currentIteration).doWrite(list);
	// }

	public static void storeFEData(String instance, String algName, List<TwoTuple<Integer, Double>> list) {
		String directory = FileUtil.getFilePath() + ResulPrefix + algName + "\\";
		// 创建目录
		File file = new File(directory);
		file.mkdirs();
		// 存储数据
		List<FEResult> results = new ArrayList<FEResult>();

		for (int i = 0; i < list.size(); i++) {
			FEResult result = new FEResult();
			result.setNfe(list.get(i).first);
			result.setObjectiveValue(list.get(i).second);
			results.add(result);
		}
		String filePath = directory + instance + "_FE" + ExcelSuffix;
		EasyExcel.write(filePath, FEResult.class).sheet("FE").doWrite(results);
	}
}
