package util;

import java.util.Random;

/**
 * 利用Java中现有的类实现高斯随机数
 * 
 * @author Administrator
 *
 */
public class GaussRandom {
	private Random random = new Random();
	double mean;// 均值
	double variance;// 方差

	/**
	 * 默认为N(0,1)
	 */
	public GaussRandom() {
		mean = 0.0;
		variance = 1.0;
	}

	public GaussRandom(double mean, double variance) {
		this.mean = mean;
		this.variance = variance;
	}

	/**
	 * 返回一个均值为mean,方差为variance的高斯随机数
	 * 
	 * @return
	 */
	public double nextGuassianDouble() {
		double result = Math.sqrt(variance) * random.nextGaussian() + mean;
		return result;
	}

	public static void main(String[] args) {
		GaussRandom gaussRandom = new GaussRandom(2, 2);
		for (int i = 0; i < 100; i++) {
			if (i % 10 == 0) {
				System.out.println();
			}
			System.out.print(gaussRandom.nextGuassianDouble() + " ");
		}
	}
}
