//package solution;
//
///**
// * 对于连续优化问题1的水波优化算法解
// * 
// * @author Administrator
// *
// */
//public class WaveForContinuousProblem1 extends Wave<Number, Number> {
//
//	public WaveForContinuousProblem1(Double[] content, Double upper, Double lower, Double value, int id, int dimension,
//			int h, double lambda) {
//		super(content, upper, lower, value, id, dimension, h, lambda);
//	}
//
//	@Override
//	public boolean check() {
//		for (int i = 0; i < content.length; i++) {
//			if (content[i] < lower || content[i] > upper) {
//				return false;
//			}
//		}
//		return true;
//	}
//
//	@Override
//	public String toString() {
//		String str = "";
//		for (int i = 0; i < content.length; i++) {
//			str += content[i] + " ";
//		}
//		str += "size: " + content.length;
//		return str;
//	}
//
//}
