package solution;

/**
 * 用于解决MLSIAP的解，使用Wave表示的方式
 * 
 * @author Dehuai Liu
 *
 */
public class WaveForMLSIAP extends SIAPSolution {
	int height;// 波高
	double length;// 波长

	public WaveForMLSIAP(Integer[] content, Integer upper, Integer lower, int id, int dimension, int height,
			double length) {
		super(content, upper, lower, id, dimension);
		this.height = height;
		this.length = length;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		Integer[] newContent = new Integer[content.length];
		for (int i = 0; i < content.length; i++) {
			newContent[i] = content[i];
		}

		WaveForMLSIAP newWave = new WaveForMLSIAP(newContent, upper, lower, id, dimension, height, length);
		// 设置四个返回值
		newWave.setValue(value);
		newWave.setMaxTime(maxTime);
		newWave.setTotalS(totalS);
		newWave.setValueOfPFC(valueOfPFC);
		return newWave;
	}

}
