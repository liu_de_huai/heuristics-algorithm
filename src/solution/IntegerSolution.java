package solution;

public abstract class IntegerSolution extends Solution<Integer, Double> {

	public IntegerSolution(Integer[] content, Integer upper, Integer lower, int id, int dimension) {
		super(content, upper, lower, id, dimension);
	}

}
