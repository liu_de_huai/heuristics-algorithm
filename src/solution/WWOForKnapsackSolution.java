//package solution;
//
//import java.util.Arrays;
//
//public class WWOForKnapsackSolution extends Wave<Integer> {
//
//	public WWOForKnapsackSolution(Integer[] content, Integer upper, Integer lower, Integer value, int id, int dimension,
//			int h, double lambda) {
//		super(content, upper, lower, value, id, dimension, h, lambda);
//	}
//
//	/*
//	 * 背包问题中解中每一维的值只能是{0,1}
//	 */
//	@Override
//	public boolean check() {
//		for (int i = 0; i < content.length; i++) {
//			if (content[i] != 0 && content[i] != 1) {
//				return false;
//			}
//		}
//		return true;
//	}
//
//	@Override
//	public String toString() {
//		return "KnapsackSolution [content=" + Arrays.toString(content) + ", id=" + id + ", dimension=" + dimension
//				+ " size: " + content.length + "]";
//	}
//
//}
