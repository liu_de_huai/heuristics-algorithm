package solution;

import java.util.Arrays;

public class SIAPSolution extends IntegerSolution {
	double valueOfPFC;
	int totalS;
	double maxTime;

	public SIAPSolution(Integer[] content, Integer upper, Integer lower, int id, int dimension) {
		super(content, upper, lower, id, dimension);
	}

	@Override
	public boolean check() {
		return false;
	}

	public double getValueOfPFC() {
		return valueOfPFC;
	}

	public void setValueOfPFC(double valueOfPFC) {
		this.valueOfPFC = valueOfPFC;
	}

	public int getTotalS() {
		return totalS;
	}

	public void setTotalS(int totalS) {
		this.totalS = totalS;
	}

	public double getMaxTime() {
		return maxTime;
	}

	public void setMaxTime(double maxTime) {
		this.maxTime = maxTime;
	}

	@Override
	public String toString() {
		return "SIAPSolution [value=" + value + ",valueOfPFC=" + valueOfPFC + ", totalS=" + totalS + ", maxTime="
				+ maxTime + ", content=" + Arrays.toString(content) + "]";
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		Integer[] newContent = new Integer[dimension];
		for (int i = 0; i < content.length; i++) {
			newContent[i] = content[i];
		}
		SIAPSolution newSolution = new SIAPSolution(newContent, upper, lower, id, dimension);
		newSolution.setMaxTime(maxTime);
		newSolution.setTotalS(totalS);
		newSolution.setValue(value);
		newSolution.setValueOfPFC(valueOfPFC);
		return newSolution;
	}

}
