package solution;

/**
 * 水波优化算法中，一个Wave表示一个解
 * 
 * @author Administrator
 *
 */
public abstract class Wave<T1 extends Number, T2 extends Number> extends Solution<T1, T2> {

	public Wave(T1[] content, T1 upper, T1 lower, int id, int dimension) {
		super(content, upper, lower, id, dimension);
	}

	protected int h;// 波高
	protected double lambda;// 波长

	public int getH() {
		return h;
	}

	public void setH(int h) {
		this.h = h;
	}

	public double getLambda() {
		return lambda;
	}

	public void setLambda(double lambda) {
		this.lambda = lambda;
	}

}
