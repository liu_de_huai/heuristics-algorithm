package solution;

/**
 * 表示一个解
 * 
 * @author Administrator
 * @param T1指定解中的数值表示
 * @param T2指定要返回的函数值的表示
 */
public abstract class Solution<T1 extends Number, T2 extends Number> {
	protected T1[] content;// 用于保存解中的内容
	protected final T1 upper;// 上界
	protected final T1 lower;// 下界
	protected T2 value;// 函数值

	// 可以考虑使用泛型表示解的值
	// protected T value;//表示解的函数值
	protected final int id;// 解的编号
	protected final int dimension;// 问题的维度

	public Solution(T1[] content, T1 upper, T1 lower, int id, int dimension) {
		super();
		this.content = content;
		this.upper = upper;
		this.lower = lower;
		this.id = id;
		this.dimension = dimension;
	}

	public T2 getValue() {
		return value;
	}

	public void setValue(T2 value) {
		this.value = value;
	}

	public void setContent(T1[] content) {
		this.content = content;
	}

	public T1[] getContent() {
		return content;
	}

	public T1 getUpper() {
		return upper;
	}

	public T1 getLower() {
		return lower;
	}

	public int getId() {
		return id;
	}

	public int getDimension() {
		return dimension;
	}

	/**
	 * 判断解是否在定义域内
	 * 
	 * 
	 * @return
	 */
	public abstract boolean check();
}
