package solution;

public class ParticleForMLSIAP extends SIAPSolution {
	ParticleForMLSIAP pBest;// 历史最优
	double[] vs;// 速度向量

	public ParticleForMLSIAP(Integer[] content, Integer upper, Integer lower, int id, int dimension, double[] vs) {
		super(content, upper, lower, id, dimension);
		this.vs = vs;
	}

	public ParticleForMLSIAP getpBest() {
		return pBest;
	}

	public void setpBest(ParticleForMLSIAP pBest) {
		this.pBest = pBest;
	}

	public double[] getVs() {
		return vs;
	}

	public void setVs(double[] vs) {
		this.vs = vs;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		Integer[] newContent = new Integer[dimension];
		for (int i = 0; i < content.length; i++) {
			newContent[i] = content[i];
		}
		ParticleForMLSIAP particle = new ParticleForMLSIAP(newContent, upper, lower, id, dimension, vs);
		// 设置四个返回值
		particle.setValue(value);
		particle.setMaxTime(maxTime);
		particle.setTotalS(totalS);
		particle.setValueOfPFC(valueOfPFC);
		return particle;
	}

}
