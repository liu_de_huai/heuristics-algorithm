package entity;

import com.alibaba.excel.annotation.ExcelProperty;

import lombok.Data;

/**
 * 设备类型Excel类用于产生数据
 * 
 * @author Dehuai Liu
 *
 */
public class DeviceTypeExcel {
	@ExcelProperty
	int h;// 设备类型编号
	@ExcelProperty
	int nDh;// 该类型设备的数量，至少为1
	@ExcelProperty
	double phT1;
	@ExcelProperty
	double phT2;
	@ExcelProperty
	double phT3;
	@ExcelProperty
	double phT4;
	@ExcelProperty
	double qh;// P(Ah|NT)
	@ExcelProperty
	double th;// 设备类型h的单台设备在有一个检查人员的时候检查一个乘客所需的时间

	public DeviceTypeExcel() {

	}

	public DeviceTypeExcel(int h, int nDh, double phT1, double phT2, double phT3, double phT4, double qh, double th) {
		super();
		this.h = h;
		this.nDh = nDh;
		this.phT1 = phT1;
		this.phT2 = phT2;
		this.phT3 = phT3;
		this.phT4 = phT4;
		this.qh = qh;
		this.th = th;
	}

	public int getH() {
		return h;
	}

	public void setH(int h) {
		this.h = h;
	}

	public int getnDh() {
		return nDh;
	}

	public void setnDh(int nDh) {
		this.nDh = nDh;
	}

	public double getPhT1() {
		return phT1;
	}

	public void setPhT1(double phT1) {
		this.phT1 = phT1;
	}

	public double getPhT2() {
		return phT2;
	}

	public void setPhT2(double phT2) {
		this.phT2 = phT2;
	}

	public double getPhT3() {
		return phT3;
	}

	public void setPhT3(double phT3) {
		this.phT3 = phT3;
	}

	public double getPhT4() {
		return phT4;
	}

	public void setPhT4(double phT4) {
		this.phT4 = phT4;
	}

	public double getQh() {
		return qh;
	}

	public void setQh(double qh) {
		this.qh = qh;
	}

	public double getTh() {
		return th;
	}

	public void setTh(double th) {
		this.th = th;
	}

}
