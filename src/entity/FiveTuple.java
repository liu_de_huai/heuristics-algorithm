package entity;

public class FiveTuple<A, B, C, D, E> {
	A first;
	B second;
	C thrid;
	D fourth;
	E fifth;

	public FiveTuple(A first, B second, C thrid, D fourth, E fifth) {
		super();
		this.first = first;
		this.second = second;
		this.thrid = thrid;
		this.fourth = fourth;
		this.fifth = fifth;
	}

	@Override
	public String toString() {
		return "FiveTuple [first=" + first + ", second=" + second + ", thrid=" + thrid + ", fourth=" + fourth
				+ ", fifth=" + fifth + "]";
	}

}
