package entity;

import com.alibaba.excel.annotation.ExcelProperty;

/**
 * 用于存储实验数据
 * 
 * @author Dehuai Liu
 *
 */
public class Result {
	@ExcelProperty("ObjectiveValue")
	double objectiveValue;
	@ExcelProperty("ValueOfPFC")
	double valueOfPFC;
	@ExcelProperty("TotalS")
	int totalS;
	@ExcelProperty("MaxTime")
	double maxTime;
	@ExcelProperty("RunTime")
	double runTime;

	public Result() {

	}

	public double getObjectiveValue() {
		return objectiveValue;
	}

	public void setObjectiveValue(double objectiveValue) {
		this.objectiveValue = objectiveValue;
	}

	public double getValueOfPFC() {
		return valueOfPFC;
	}

	public void setValueOfPFC(double valueOfPFC) {
		this.valueOfPFC = valueOfPFC;
	}

	public int getTotalS() {
		return totalS;
	}

	public void setTotalS(int totalS) {
		this.totalS = totalS;
	}

	public double getMaxTime() {
		return maxTime;
	}

	public void setMaxTime(double maxTime) {
		this.maxTime = maxTime;
	}

	public double getRunTime() {
		return runTime;
	}

	public void setRunTime(double runTime) {
		this.runTime = runTime;
	}

}
