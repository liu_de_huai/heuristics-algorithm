package entity;

import com.alibaba.excel.annotation.ExcelProperty;

/**
 * 用于产生问题实例的Excel表
 * 
 * @author Dehuai Liu
 *
 */
public class MLSIAPSetting {
	@ExcelProperty("Id")
	private Integer instanceNo;
	@ExcelProperty("N1")
	private Integer n1;// 普通乘客的数量
	@ExcelProperty("N2")
	private Integer n2;// 风险乘客的数量
	@ExcelProperty("M1")
	private Integer m1;// 强制检查设备类型的数量
	@ExcelProperty("M2")
	private Integer m2;// 特殊检查设备类型的数量
	@ExcelProperty("K")
	Integer k;
	@ExcelProperty("T")
	double t;
	@ExcelProperty("S")
	private Integer s;// 检查人员人数上限
	@ExcelProperty
	private Double omega;// 风险系数
	@ExcelProperty
	private Double delta;// P(FC)上界
	@ExcelProperty("NFE")
	int nfe;
	@ExcelProperty("NP")
	int np;// 该问题实例对应的种群大小

	public MLSIAPSetting() {

	}

	public MLSIAPSetting(Integer instanceNo, Integer n1, Integer n2, Integer m1, Integer m2, Integer k, double t,
			Integer s, Double omega, Double delta, int nfe, int np) {
		super();
		this.instanceNo = instanceNo;
		this.n1 = n1;
		this.n2 = n2;
		this.m1 = m1;
		this.m2 = m2;
		this.k = k;
		this.t = t;
		this.s = s;
		this.omega = omega;
		this.delta = delta;
		this.nfe = nfe;
		this.np = np;
	}

	public Integer getInstanceNo() {
		return instanceNo;
	}

	public void setInstanceNo(Integer instanceNo) {
		this.instanceNo = instanceNo;
	}

	public Integer getN1() {
		return n1;
	}

	public void setN1(Integer n1) {
		this.n1 = n1;
	}

	public Integer getN2() {
		return n2;
	}

	public void setN2(Integer n2) {
		this.n2 = n2;
	}

	public Integer getM1() {
		return m1;
	}

	public void setM1(Integer m1) {
		this.m1 = m1;
	}

	public Integer getM2() {
		return m2;
	}

	public void setM2(Integer m2) {
		this.m2 = m2;
	}

	public Integer getK() {
		return k;
	}

	public void setK(Integer k) {
		this.k = k;
	}

	public double getT() {
		return t;
	}

	public void setT(double t) {
		this.t = t;
	}

	public Integer getS() {
		return s;
	}

	public void setS(Integer s) {
		this.s = s;
	}

	public Double getOmega() {
		return omega;
	}

	public void setOmega(Double omega) {
		this.omega = omega;
	}

	public Double getDelta() {
		return delta;
	}

	public void setDelta(Double delta) {
		this.delta = delta;
	}

	public int getNfe() {
		return nfe;
	}

	public void setNfe(int nfe) {
		this.nfe = nfe;
	}

	public int getNp() {
		return np;
	}

	public void setNp(int np) {
		this.np = np;
	}

}
