package entity;

/**
 * 表示一种设备类型
 * 
 * @author Dehuai Liu
 *
 */
public class DeviceType {
	final int h;// 设备类型编号
	int NDh;// 该类型设备的数量，至少为1
	final double[] phTk;// P(Ah|Tk)
	final double qh;// P(Ah|NT)
	int Sh;// 为设备类型h分配的检查人员数，至少为1
	final double Th;// 设备类型h的单台设备在有一个检查人员的时候检查一个乘客所需的时间
	double th;// 每次为该设备类型分配完检查人员之后，都要重新计算该时间

	double timeOfInspectPassengers;//一个设备检查乘客的总时间,每次重新分配乘客后，需要重新计算

	public DeviceType(int h, int NDh, double[] phTk, double qh, int Sh, double Th) {
		super();
		this.h = h;
		this.NDh = NDh;
		this.phTk = phTk;
		this.qh = qh;
		this.Sh = Sh;
		this.Th = Th;
	}

	public int getSh() {
		return Sh;
	}

	public void setSh(int Sh) {
		this.Sh = Sh;
	}

	public double getTh() {
		return th;
	}

	public void setTh(double th) {
		this.th = th;
	}

	public int getH() {
		return h;
	}

	public int getNDh() {
		return NDh;
	}

	public void setNDh(int nDh) {
		NDh = nDh;
	}

	public double[] getPhTk() {
		return phTk;
	}

	public double getQh() {
		return qh;
	}

	public double getConstTh() {
		return Th;
	}

	public double getTimeOfInspectPassengers() {
		return timeOfInspectPassengers;
	}

	public void setTimeOfInspectPassengers(double timeOfInspectPassengers) {
		this.timeOfInspectPassengers = timeOfInspectPassengers;
	}

}
