package entity;

import com.alibaba.excel.annotation.ExcelProperty;

public class FEResult {
	@ExcelProperty("NFE")
	int nfe;
	@ExcelProperty("ObjectiveValue")
	double objectiveValue;

	public FEResult() {
	}

	public int getNfe() {
		return nfe;
	}

	public void setNfe(int nfe) {
		this.nfe = nfe;
	}

	public double getObjectiveValue() {
		return objectiveValue;
	}

	public void setObjectiveValue(double objectiveValue) {
		this.objectiveValue = objectiveValue;
	}

}
