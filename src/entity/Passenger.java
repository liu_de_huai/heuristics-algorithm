package entity;

import java.util.Arrays;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;

/**
 * 表示一个乘客实体
 * 
 * @author Dehuai Liu
 *
 */
public class Passenger {
	@ExcelProperty
	int id;// 乘客编号
	@ExcelProperty("P(T|Pi)")
	double piT;
	@ExcelProperty
	int numOfDevices;
	/*
	 * thi中，设备h是从1开始编号的
	 */
	@ExcelIgnore
	double[] thi;// 乘客在每个站点完成检查的时间，还要包括t0i
	@ExcelIgnore
	int xi;// 乘客i分配到的分组

	public Passenger() {
	}

	public Passenger(int id, double piT, int numOfDevices) {
		this.id = id;
		this.piT = piT;
		this.numOfDevices = numOfDevices;
	}

	/**
	 * 得到乘客id
	 * 
	 * @return
	 */
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getPiT() {
		return piT;
	}

	public void setPiT(double piT) {
		this.piT = piT;
	}

	/**
	 * 返回乘客i在设备h检查完毕的时间
	 * 
	 * @param h
	 * @return
	 */
	public double getTimeOfDeviceH(int h) {
		return thi[h];
	}

	public void setTimeOfDeviceH(int h, double time) {
		thi[h] = time;
	}

	public int getXi() {
		return xi;
	}

	public void setXi(int xi) {
		this.xi = xi;
	}

	public double[] getThi() {
		return thi;
	}

	public void setThi(double[] thi) {
		this.thi = thi;
	}

	public int getNumOfDevices() {
		return numOfDevices;
	}

	public void setNumOfDevices(int numOfDevices) {
		this.numOfDevices = numOfDevices;
	}

	@Override
	public String toString() {
		return "Passenger [id=" + id + ", piT=" + piT + ", numOfDevices=" + numOfDevices + ", thi="
				+ Arrays.toString(thi) + ", xi=" + xi + "]";
	}

}
