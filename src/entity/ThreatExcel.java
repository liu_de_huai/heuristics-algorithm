package entity;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.annotation.ExcelProperty;

import util.FileUtil;

/**
 * 威胁Excel类
 * 
 * @author Dehuai Liu
 *
 */
public class ThreatExcel {
	@ExcelProperty("T1")
	double t1;
	@ExcelProperty("T2")
	double t2;
	@ExcelProperty("T3")
	double t3;
	@ExcelProperty("T4")
	double t4;

	public ThreatExcel() {
		t1 = 0.05;
		t2 = 0.15;
		t3 = 0.4;
		t4 = 0.4;
	}

	public double getT1() {
		return t1;
	}

	public void setT1(double t1) {
		this.t1 = t1;
	}

	public double getT2() {
		return t2;
	}

	public void setT2(double t2) {
		this.t2 = t2;
	}

	public double getT3() {
		return t3;
	}

	public void setT3(double t3) {
		this.t3 = t3;
	}

	public double getT4() {
		return t4;
	}

	public void setT4(double t4) {
		this.t4 = t4;
	}

	public static void main(String[] args) {
		ThreatExcel threat = new ThreatExcel();
		List<ThreatExcel> list = new ArrayList<ThreatExcel>();
		list.add(threat);
		String filePath = FileUtil.getFilePath() + "Threat\\4threat.xlsx";
		EasyExcel.write(filePath, ThreatExcel.class).sheet("威胁").doWrite(list);

	}
}
