package entity;

import com.alibaba.excel.annotation.ExcelProperty;

public class Ndh {
	int instanceId;
	@ExcelProperty("ND1")
	int nD1;
	@ExcelProperty("ND2")
	int nD2;
	@ExcelProperty("ND3")
	int nD3;
	@ExcelProperty("ND4")
	int nD4;
	@ExcelProperty("ND5")
	int nD5;
	@ExcelProperty("ND6")
	int nD6;
	@ExcelProperty("ND7")
	int nD7;
	@ExcelProperty("ND8")
	int nD8;

	public Ndh() {

	}

	public Ndh(int instanceId, int nD1, int nD2, int nD3, int nD4, int nD5, int nD6, int nD7, int nD8) {
		super();
		this.instanceId = instanceId;
		this.nD1 = nD1;
		this.nD2 = nD2;
		this.nD3 = nD3;
		this.nD4 = nD4;
		this.nD5 = nD5;
		this.nD6 = nD6;
		this.nD7 = nD7;
		this.nD8 = nD8;
	}

	public int getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(int instanceId) {
		this.instanceId = instanceId;
	}

	public int getnD1() {
		return nD1;
	}

	public void setnD1(int nD1) {
		this.nD1 = nD1;
	}

	public int getnD2() {
		return nD2;
	}

	public void setnD2(int nD2) {
		this.nD2 = nD2;
	}

	public int getnD3() {
		return nD3;
	}

	public void setnD3(int nD3) {
		this.nD3 = nD3;
	}

	public int getnD4() {
		return nD4;
	}

	public void setnD4(int nD4) {
		this.nD4 = nD4;
	}

	public int getnD5() {
		return nD5;
	}

	public void setnD5(int nD5) {
		this.nD5 = nD5;
	}

	public int getnD6() {
		return nD6;
	}

	public void setnD6(int nD6) {
		this.nD6 = nD6;
	}

	public int getnD7() {
		return nD7;
	}

	public void setnD7(int nD7) {
		this.nD7 = nD7;
	}

	public int getnD8() {
		return nD8;
	}

	public void setnD8(int nD8) {
		this.nD8 = nD8;
	}

}
