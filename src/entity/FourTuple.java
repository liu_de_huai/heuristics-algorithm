package entity;

public class FourTuple<A, B, C, D> {
	A first;
	B second;
	C thrid;
	D fourth;

	public FourTuple(A first, B second, C thrid, D fourth) {
		super();
		this.first = first;
		this.second = second;
		this.thrid = thrid;
		this.fourth = fourth;
	}

	public A getFirst() {
		return first;
	}

	public void setFirst(A first) {
		this.first = first;
	}

	public B getSecond() {
		return second;
	}

	public void setSecond(B second) {
		this.second = second;
	}

	public C getThrid() {
		return thrid;
	}

	public void setThrid(C thrid) {
		this.thrid = thrid;
	}

	public D getFourth() {
		return fourth;
	}

	public void setFourth(D fourth) {
		this.fourth = fourth;
	}

	@Override
	public String toString() {
		return "FourTuple [first=" + first + ", second=" + second + ", thrid=" + thrid + ", fourth=" + fourth + "]";
	}

}
