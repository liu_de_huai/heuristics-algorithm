package entity;

/**
 * 封装两个对象的二元组
 * 
 * @author Administrator
 *
 */
public class TwoTuple<A, B> {
	public A first;
	public B second;

	public TwoTuple(A first, B second) {
		this.first = first;
		this.second = second;
	}

	@Override
	public String toString() {
		return "TwoTuple [first=" + first + ", second=" + second + "]";
	}

}
