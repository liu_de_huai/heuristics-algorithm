package test;

import java.util.Random;

public class Test {

	public static void main(String[] args) {
		// test1();
		// test2();
		// test3();
		// test4();
		// test5();
		test6();
	}

	private static void test6() {
		System.out.println(8 / 2);
		System.out.println(7 / 2);
		System.out.println(Math.round(56.0 / 20));
		System.out.println(0.5 * 1 / 1.0026);
		System.out.println(0.5 * Math.pow(1.0026, -1000));
		System.out.println(0.5 * Math.pow(1.0026, -(0.000000009 / 5 + 0.000000009) * 1000));
	}

	private static void test5() {
		int i = 1;
		i += 1 * 2;
		System.out.println(i);
	}

	private static void test4() {
		Random r = new Random();
		for (int i = 0; i < 100; i++) {
			System.out.println(r.nextGaussian());
		}
		System.out.println(Math.round(-0.56));
	}

	private static void test3() {
		long startTime = System.currentTimeMillis();
		try {
			Thread.currentThread().sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		long endTime = System.currentTimeMillis();
		System.out.println(endTime - startTime);
	}

	private static void test2() {
		double[][] arr = new double[2][3];
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[0].length; j++) {
				System.out.print(arr[i][j] + " ");
			}
			System.out.println();
		}
	}

	private static void test1() {
		Integer i = 1;
		Integer[] arr = { 1, 2, 3 };
		for (Integer integer : arr) {
			System.out.println(integer);
		}
	}

}

class A<T> {
	T a;

	public T getA() {
		return a;
	}

	public void setA(T a) {
		this.a = a;
	}
}

class B extends A<Integer> {

}