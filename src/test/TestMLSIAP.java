package test;

import java.util.Random;

import entity.FourTuple;
import problem.MLSIAP;
import solution.SIAPSolution;
import solution.Solution;

public class TestMLSIAP {

	public static void main(String[] args) {
		for (int i = 1; i <= 8; i++) {
			test1(i, 15);
			test1(i, 0);
		}
	}

	private static void test1(int instanceId, int groupNum) {
		Random random = new Random();
		MLSIAP instance = MLSIAP.getInstance(instanceId);
		int N1 = instance.getN1();
		int N2 = instance.getN2();
		int M1 = instance.getM1();
		int M2 = instance.getM2();
		Integer[] content = new Integer[N1 + N2 + M1 + M2];
		int i = 0;
		for (; i < N1 + N2; i++) {
			content[i] = groupNum;
		}
		
		instance.calTotolTimeOfEveryDevice(content);
		
		for (; i < N1 + N2 + M1 + M2; i++) {
			content[i] = random.nextInt(3);
		}
		Solution<Integer, Double> solution = new SIAPSolution(content, 15, 0, 0, N1 + N2 + M1 + M2);
		FourTuple<Double, Double, Integer, Double> result = instance.calculate(solution);
		System.out.println(result);
	}

}
