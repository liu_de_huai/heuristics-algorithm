package test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;

public class TestEasyExcel {

	/**
	 * 用DemoData保存一行数据，添加到List中，List保存了多行数据
	 * 
	 * @return
	 */
	private List<DemoData> data() {
		List<DemoData> list = new ArrayList<DemoData>();
		for (int i = 0; i < 10; i++) {
			DemoData data = new DemoData();
			data.setString("字符串" + i);
			data.setDate(new Date());
			data.setDoubleData(0.56);
			list.add(data);
		}
		return list;
	}

	/**
	 * 数据的写入
	 */
	@Test
	public void simpleWrite() {
		// 指定文件名
		String fileName = "E:\\ldh\\文件\\研究生\\论文\\data\\problem\\test.xlsx";

		// 方法1
		// EasyExcel.write(fileName, DemoData.class).sheet("测试").doWrite(data());

		// 方法2
		// 这里 需要指定写用哪个class去写
		ExcelWriter excelWriter = EasyExcel.write(fileName, DemoData.class).build();
		WriteSheet writeSheet = EasyExcel.writerSheet("模板").build();
		excelWriter.write(data(), writeSheet);
		/// 千万别忘记finish 会帮忙关闭流
		excelWriter.finish();
	}

	/**
	 * 数据的读取
	 */
	@Test
	public void simpleRead() {
		// 指定文件名
		String fileName = "E:\\ldh\\文件\\研究生\\论文\\data\\problem\\test.xlsx";
		
		DemoDataListener listener=new DemoDataListener();
		//读取数据
		EasyExcel.read(fileName,DemoData.class,listener).sheet().doRead();
		
		//
		List<DemoData> list=listener.getList();
		for(DemoData data:list) {
			System.out.println(data);
		}
	}
}
