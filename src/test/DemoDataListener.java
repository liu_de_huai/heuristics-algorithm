package test;
/**
 * 针对DemoData数据类型的监听器，在读取数据的时候用到。
 * @author Dehuai Liu
 *
 */

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.fastjson.JSON;

public class DemoDataListener extends AnalysisEventListener<DemoData> {
	//private static final Logger LOGGER = LoggerFactory.getLogger(DemoDataListener.class);

	List<DemoData> list = new ArrayList<DemoData>();//用于存储读取出的数据

	public DemoDataListener() {

	}

	public List<DemoData> getList() {
		return list;
	}

	/**
	 * 每一条数据解析都会调用invoke方法
	 */
	@Override
	public void invoke(DemoData data, AnalysisContext context) {
		//LOGGER.info("解析到一条数据：{}", JSON.toJSONString(data));
		list.add(data);
	}

	/**
	 * 所有数据解析完成后，调用该方法
	 */
	@Override
	public void doAfterAllAnalysed(AnalysisContext context) {

	}

}
