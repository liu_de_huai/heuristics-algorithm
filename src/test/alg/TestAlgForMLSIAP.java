package test.alg;

import java.util.ArrayList;
import java.util.List;

import algorithm.AdaptedWWOForMLSIAP;
import algorithm.AdaptedWWOForMLSIAP1;
import algorithm.AdaptedWWOForMLSIAP2;
import algorithm.AdaptedWWOForMLSIAP3;
import algorithm.DEAlgForMLSIAP;
import algorithm.GAForMLSIAP;
import algorithm.PSOAlgForMLSIAP;
import algorithm.SimpleWWOAlgForMLSIAP;
import algorithm.WWOAlgForMLSIAP;
import entity.TwoTuple;
import problem.MLSIAP;
import solution.ParticleForMLSIAP;
import solution.SIAPSolution;
import solution.WaveForMLSIAP;
import util.FileUtil;

public class TestAlgForMLSIAP {
	public static void main(String[] args) {
		// testWWO(1);

		// testSimWWO(1);

		// 基础版WWO实验
		// for(int i=1;i<=8;i++) {
		// testAdaptedWWO(i);
		// }

		// 在基础版WWO上，改进breaking
		// for (int i = 1; i <= 8; i++) {
		// testAdaptedWWO1(i);
		// }

		// 改进refraction，增加DE
		// for (int i = 1; i <= 8; i++) {
		// testAdaptedWWO2(i);
		// }

		//在AdaptedWWO１之，上删除refraction之后的波长更新公式
		for (int i = 1; i <= 8; i++) {
			testAdaptedWWO3(i);
		}
		// DE测试
		// testDE(1);

		// 测试PSO
		// for (int i = 1; i <= 8; i++) {
		// testPSOAlgForMLSIAP(i);
		// }

		// GA测试
		// for (int i = 4; i <= 8; i++) {
		// testGAForMLSIAP(i);
		// }

	}

	private static void testAdaptedWWO3(int instanceId) {
		// 获得每一次运行的最优解和运行时间
		List<TwoTuple<SIAPSolution, Double>> list = new ArrayList<TwoTuple<SIAPSolution, Double>>();
		// 获得问题实例
		MLSIAP instance = MLSIAP.getInstance(instanceId);
		// 实验参数设置
		String algName = "AdaptedWWO3";
		int iteration = 1;
		int hmax = 12;
		double alpha = 1.0026;
		double betaMax = Math.max(1, instance.getDimension() / 200);
		double betaMin = 1;
		int kmax = 12;
		int MaxNFE = instance.getNFE();
		int NP = instance.getNP();
		double lambdaMax = 0.5;
		double lambdaMin = 0.035;
		double upsilon = 0.5;
		List<TwoTuple<Integer, Double>> statisticalList = new ArrayList<TwoTuple<Integer, Double>>();

		for (int i = 0; i < iteration; i++) {
			AdaptedWWOForMLSIAP3 alg = new AdaptedWWOForMLSIAP3(instance, false, algName, NP, MaxNFE, hmax, alpha,
					betaMax, betaMin, kmax, lambdaMax, lambdaMin, upsilon);
			System.out.println(alg.getAlgName() + " 第" + i + "次迭代");
			TwoTuple<WaveForMLSIAP, Double> result = alg.run();
			TwoTuple<SIAPSolution, Double> sresult = convert1(result);
			list.add(sresult);

			// 得到算法每1000次迭代过程的数据
			List<TwoTuple<Integer, WaveForMLSIAP>> list2 = alg.getRecords();
			if (i == 0) {
				for (TwoTuple<Integer, WaveForMLSIAP> tuple : list2) {
					statisticalList.add(new TwoTuple<Integer, Double>(tuple.first, tuple.second.getValue()));
				}
			} else {
				for (int j = 0; j < list2.size(); j++) {
					statisticalList.get(j).second += list2.get(j).second.getValue();
				}
			}
		}

		for (TwoTuple<Integer, Double> tuple : statisticalList) {
			tuple.second = tuple.second / iteration;
		}
		//FileUtil.storeFEData("instance" + instanceId, algName, statisticalList);
		// 计算总时间
		double totalTime = 0.0;
		for (int i = 0; i < iteration; i++) {
			totalTime += list.get(i).second;
		}

		for (TwoTuple<SIAPSolution, Double> tuple : list) {
			System.out.println(tuple);
		}
		System.out.println("总的运行时间为：" + totalTime + "s");
		String instanceName = "instance" + instanceId;
		//FileUtil.storeExperimentData(algName, instanceName, list);
	}

	private static void testGAForMLSIAP(int instanceId) {
		List<TwoTuple<SIAPSolution, Double>> list = new ArrayList<TwoTuple<SIAPSolution, Double>>();
		MLSIAP instance = MLSIAP.getInstance(instanceId);
		// 实验参数设置
		String algName = "GA";
		int iteration = 30;
		int MaxNFE = instance.getNFE();
		int NP = 30;
		double Pc = 0.9;
		double Pm = 0.03;

		List<TwoTuple<Integer, Double>> statisticalList = new ArrayList<TwoTuple<Integer, Double>>();

		for (int i = 0; i < iteration; i++) {
			GAForMLSIAP alg = new GAForMLSIAP(instance, false, algName, NP, MaxNFE, Pc, Pm);
			System.out.println(alg.getAlgName() + " 第" + i + "次迭代");
			TwoTuple<SIAPSolution, Double> result = alg.run();
			list.add(result);

			List<TwoTuple<Integer, SIAPSolution>> list2 = alg.getRecords();
			if (i == 0) {
				for (TwoTuple<Integer, SIAPSolution> tuple : list2) {
					statisticalList.add(new TwoTuple<Integer, Double>(tuple.first, tuple.second.getValue()));
				}
			} else {
				for (int j = 0; j < list2.size(); j++) {
					statisticalList.get(j).second += list2.get(j).second.getValue();
				}
			}
		}

		for (TwoTuple<Integer, Double> tuple : statisticalList) {
			tuple.second = tuple.second / iteration;
		}
		FileUtil.storeFEData("instance" + instanceId, algName, statisticalList);

		// 计算总时间
		double totalTime = 0.0;
		for (int i = 0; i < iteration; i++) {
			totalTime += list.get(i).second;
		}

		for (TwoTuple<SIAPSolution, Double> tuple : list) {
			System.out.println(tuple);
		}
		System.out.println("总的运行时间为：" + totalTime + "s");
		String instanceName = "instance" + instanceId;
		FileUtil.storeExperimentData(algName, instanceName, list);
	}

	private static void testPSOAlgForMLSIAP(int instanceId) {
		List<TwoTuple<SIAPSolution, Double>> list = new ArrayList<TwoTuple<SIAPSolution, Double>>();
		MLSIAP instance = MLSIAP.getInstance(instanceId);
		// 实验参数设置
		String algName = "PSO";
		int iteration = 30;
		int MaxNFE = instance.getNFE();
		int NP = 30;
		int Vmax = 6;
		double wMax = 0.9;
		double wMin = 0.4;

		double c1 = 1.49445, c2 = 1.49445;

		List<TwoTuple<Integer, Double>> statisticalList = new ArrayList<TwoTuple<Integer, Double>>();

		for (int i = 0; i < iteration; i++) {
			PSOAlgForMLSIAP alg = new PSOAlgForMLSIAP(instance, false, algName, NP, MaxNFE, wMax, wMin, c1, c2, Vmax);
			System.out.println(alg.getAlgName() + " 第" + i + "次迭代");
			TwoTuple<ParticleForMLSIAP, Double> result = alg.run();
			TwoTuple<SIAPSolution, Double> cresult = new TwoTuple<SIAPSolution, Double>(result.first, result.second);
			list.add(cresult);
			// 得到算法每1000次迭代过程的数据
			List<TwoTuple<Integer, ParticleForMLSIAP>> list2 = alg.getRecords();
			if (i == 0) {
				for (TwoTuple<Integer, ParticleForMLSIAP> tuple : list2) {
					statisticalList.add(new TwoTuple<Integer, Double>(tuple.first, tuple.second.getValue()));
				}
			} else {
				for (int j = 0; j < list2.size(); j++) {
					statisticalList.get(j).second += list2.get(j).second.getValue();
				}
			}
		}

		for (TwoTuple<Integer, Double> tuple : statisticalList) {
			tuple.second = tuple.second / iteration;
		}
		FileUtil.storeFEData("instance" + instanceId, algName, statisticalList);

		// 计算总时间
		double totalTime = 0.0;
		for (int i = 0; i < iteration; i++) {
			totalTime += list.get(i).second;
		}

		for (TwoTuple<SIAPSolution, Double> tuple : list) {
			System.out.println(tuple);
		}
		System.out.println("总的运行时间为：" + totalTime + "s");
		String instanceName = "instance" + instanceId;
		FileUtil.storeExperimentData(algName, instanceName, list);
	}

	private static void testAdaptedWWO2(int instanceId) {
		List<TwoTuple<SIAPSolution, Double>> list = new ArrayList<TwoTuple<SIAPSolution, Double>>();
		MLSIAP instance = MLSIAP.getInstance(instanceId);
		// 实验参数设置
		String algName = "AdaptedWWO2";
		int iteration = 30;
		int hmax = 12;
		double alpha = 1.0026;
		// double alpha = 1.0001;
		double betaMax = 1;
		double betaMin = 1;
		int kmax = 12;
		int MaxNFE = instance.getNFE();
		int NP = instance.getNP();
		double lambdaMax = 0.5;
		double lambdaMin = 0.05;
		double upsilon = 0.5;
		double F = 0.9;
		double CR = 0.1;

		List<TwoTuple<Integer, Double>> statisticalList = new ArrayList<TwoTuple<Integer, Double>>();

		for (int i = 0; i < iteration; i++) {
			AdaptedWWOForMLSIAP2 alg = new AdaptedWWOForMLSIAP2(instance, false, algName, NP, MaxNFE, hmax, alpha,
					betaMax, betaMin, kmax, lambdaMax, lambdaMin, upsilon, F, CR);
			System.out.println(alg.getAlgName() + " 第" + i + "次迭代");
			TwoTuple<WaveForMLSIAP, Double> result = alg.run();
			TwoTuple<SIAPSolution, Double> cresult = convert1(result);
			list.add(cresult);

			// 得到算法每1000次迭代过程的数据
			List<TwoTuple<Integer, WaveForMLSIAP>> list2 = alg.getRecords();
			if (i == 0) {
				for (TwoTuple<Integer, WaveForMLSIAP> tuple : list2) {
					statisticalList.add(new TwoTuple<Integer, Double>(tuple.first, tuple.second.getValue()));
				}
			} else {
				for (int j = 0; j < list2.size(); j++) {
					statisticalList.get(j).second += list2.get(j).second.getValue();
				}
			}
		}

		for (TwoTuple<Integer, Double> tuple : statisticalList) {
			tuple.second = tuple.second / iteration;
		}
		FileUtil.storeFEData("instance" + instanceId, algName, statisticalList);

		// 计算总时间
		double totalTime = 0.0;
		for (int i = 0; i < iteration; i++) {
			totalTime += list.get(i).second;
		}

		for (TwoTuple<SIAPSolution, Double> tuple : list) {
			System.out.println(tuple);
		}
		System.out.println("总的运行时间为：" + totalTime + "s");
		String instanceName = "instance" + instanceId;
		FileUtil.storeExperimentData(algName, instanceName, list);

	}

	private static void testDE(int instanceId) {
		// 获得每一次运行的最优解和运行时间
		List<TwoTuple<SIAPSolution, Double>> list = new ArrayList<TwoTuple<SIAPSolution, Double>>();
		// 获得问题实例
		MLSIAP instance = MLSIAP.getInstance(instanceId);
		// 实验参数设置
		int iteration = 30;
		String algName = "DE";
		int MaxNFE = instance.getNFE();
		int NP = 50;
		double F = 1;
		double CR = 0.25;
		for (int i = 0; i < iteration; i++) {
			DEAlgForMLSIAP alg = new DEAlgForMLSIAP(instance, false, algName, NP, MaxNFE, F, CR);
			System.out.println(alg.getAlgName() + " 第" + i + "次迭代");
			TwoTuple<SIAPSolution, Double> result = alg.run();
			list.add(result);

			// 得到算法每1000次迭代过程的数据
			List<TwoTuple<Integer, SIAPSolution>> list2 = alg.getRecords();

		}
		// 计算总时间
		double totalTime = 0.0;
		for (int i = 0; i < iteration; i++) {
			totalTime += list.get(i).second;
		}

		for (TwoTuple<SIAPSolution, Double> tuple : list) {
			System.out.println(tuple);
		}
		System.out.println("总的运行时间为：" + totalTime + "s");
		String instanceName = "instance" + instanceId;
		FileUtil.storeExperimentData(algName, instanceName, list);
	}

	private static void testAdaptedWWO1(int instanceId) {
		// 获得每一次运行的最优解和运行时间
		List<TwoTuple<SIAPSolution, Double>> list = new ArrayList<TwoTuple<SIAPSolution, Double>>();
		// 获得问题实例
		MLSIAP instance = MLSIAP.getInstance(instanceId);
		// 实验参数设置
		String algName = "AdaptedWWO1";
		int iteration = 30;
		int hmax = 12;
		double alpha = 1.0026;
		double betaMax = Math.max(1, instance.getDimension() / 200);
		double betaMin = 1;
		int kmax = 12;
		int MaxNFE = instance.getNFE();
		int NP = instance.getNP();
		double lambdaMax = 0.5;
		double lambdaMin = 0.035;
		double upsilon = 0.5;
		List<TwoTuple<Integer, Double>> statisticalList = new ArrayList<TwoTuple<Integer, Double>>();

		for (int i = 0; i < iteration; i++) {
			AdaptedWWOForMLSIAP1 alg = new AdaptedWWOForMLSIAP1(instance, false, algName, NP, MaxNFE, hmax, alpha,
					betaMax, betaMin, kmax, lambdaMax, lambdaMin, upsilon);
			System.out.println(alg.getAlgName() + " 第" + i + "次迭代");
			TwoTuple<WaveForMLSIAP, Double> result = alg.run();
			TwoTuple<SIAPSolution, Double> sresult = convert1(result);
			list.add(sresult);

			// 得到算法每1000次迭代过程的数据
			List<TwoTuple<Integer, WaveForMLSIAP>> list2 = alg.getRecords();
			if (i == 0) {
				for (TwoTuple<Integer, WaveForMLSIAP> tuple : list2) {
					statisticalList.add(new TwoTuple<Integer, Double>(tuple.first, tuple.second.getValue()));
				}
			} else {
				for (int j = 0; j < list2.size(); j++) {
					statisticalList.get(j).second += list2.get(j).second.getValue();
				}
			}
		}

		for (TwoTuple<Integer, Double> tuple : statisticalList) {
			tuple.second = tuple.second / iteration;
		}
		FileUtil.storeFEData("instance" + instanceId, algName, statisticalList);
		// 计算总时间
		double totalTime = 0.0;
		for (int i = 0; i < iteration; i++) {
			totalTime += list.get(i).second;
		}

		for (TwoTuple<SIAPSolution, Double> tuple : list) {
			System.out.println(tuple);
		}
		System.out.println("总的运行时间为：" + totalTime + "s");
		String instanceName = "instance" + instanceId;
		FileUtil.storeExperimentData(algName, instanceName, list);
	}

	private static TwoTuple<Integer, SIAPSolution> convert2(TwoTuple<Integer, WaveForMLSIAP> tuple) {
		return new TwoTuple<Integer, SIAPSolution>(tuple.first, tuple.second);
	}

	private static TwoTuple<SIAPSolution, Double> convert1(TwoTuple<WaveForMLSIAP, Double> result) {
		return new TwoTuple<SIAPSolution, Double>(result.first, result.second);
	}

	private static void testAdaptedWWO(int instanceId) {
		List<TwoTuple<SIAPSolution, Double>> list = new ArrayList<TwoTuple<SIAPSolution, Double>>();
		MLSIAP instance = MLSIAP.getInstance(instanceId);
		// 实验参数设置
		// String algName = "AdaptedWWO";
		String algName = "AdaptedWWO_hmax12";
		int iteration = 30;
		int hmax = 6;
		double alpha = 1.0026;
		// double alpha = 1.0001;
		double betaMax = Math.round(instance.getDimension() / 20);
		double betaMin = 1;
		int kmax = 12;
		int MaxNFE = instance.getNFE();
		int NP = instance.getNP();
		double lambdaMax = 0.5;
		double lambdaMin = 0.05;
		double upsilon = 0.5;

		List<TwoTuple<Integer, Double>> statisticalList = new ArrayList<TwoTuple<Integer, Double>>();

		for (int i = 0; i < iteration; i++) {
			AdaptedWWOForMLSIAP alg = new AdaptedWWOForMLSIAP(instance, false, algName, NP, MaxNFE, hmax, alpha,
					betaMax, betaMin, kmax, lambdaMax, lambdaMin, upsilon);
			System.out.println(alg.getAlgName() + " 第" + i + "次迭代");
			TwoTuple<WaveForMLSIAP, Double> result = alg.run();
			TwoTuple<SIAPSolution, Double> cresult = convert1(result);
			list.add(cresult);

			// 得到算法每1000次迭代过程的数据
			List<TwoTuple<Integer, WaveForMLSIAP>> list2 = alg.getRecords();
			if (i == 0) {
				for (TwoTuple<Integer, WaveForMLSIAP> tuple : list2) {
					statisticalList.add(new TwoTuple<Integer, Double>(tuple.first, tuple.second.getValue()));
				}
			} else {
				for (int j = 0; j < list2.size(); j++) {
					statisticalList.get(j).second += list2.get(j).second.getValue();
				}
			}
		}

		for (TwoTuple<Integer, Double> tuple : statisticalList) {
			tuple.second = tuple.second / iteration;
		}
		FileUtil.storeFEData("instance" + instanceId, algName, statisticalList);

		// 计算总时间
		double totalTime = 0.0;
		for (int i = 0; i < iteration; i++) {
			totalTime += list.get(i).second;
		}

		for (TwoTuple<SIAPSolution, Double> tuple : list) {
			System.out.println(tuple);
		}
		System.out.println("总的运行时间为：" + totalTime + "s");
		String instanceName = "instance" + instanceId;
		FileUtil.storeExperimentData(algName, instanceName, list);

	}

	private static void testWWO(int instanceId) {
		List<TwoTuple<WaveForMLSIAP, Double>> list = new ArrayList<TwoTuple<WaveForMLSIAP, Double>>();
		MLSIAP instance = MLSIAP.getInstance(instanceId);
		// 实验参数设置
		int iteration = 30;
		int hmax = 6;
		double alpha = 1.0026;
		double betaMax = 0.25;
		double betaMin = 0.04;
		int kmax = 12;
		int MaxNFE = instance.getNFE();
		int NP = 10;
		double lambdaMax = 0.5;
		double lambdaMin = 0.035;
		for (int i = 0; i < iteration; i++) {
			WWOAlgForMLSIAP alg = new WWOAlgForMLSIAP(instance, false, "WWO", NP, MaxNFE, hmax, alpha, betaMax, betaMin,
					kmax, lambdaMax, lambdaMin);
			System.out.println(alg.getAlgName() + " 第" + i + "次迭代");
			TwoTuple<WaveForMLSIAP, Double> result = alg.run();
			list.add(result);
		}
		// 计算总时间
		double totalTime = 0.0;
		for (int i = 0; i < iteration; i++) {
			totalTime += list.get(i).second;
		}

		for (TwoTuple<WaveForMLSIAP, Double> tuple : list) {
			System.out.println(tuple);
		}
		System.out.println("总的运行时间为：" + totalTime + "s");
	}

	private static void testSimWWO(int instanceId) {
		List<TwoTuple<WaveForMLSIAP, Double>> list = new ArrayList<TwoTuple<WaveForMLSIAP, Double>>();
		MLSIAP instance = MLSIAP.getInstance(instanceId);
		// 实验参数设置
		int iteration = 30;
		int hmax = 12;
		double alpha = 1.0026;
		double betaMax = 0.25;
		double betaMin = 0.001;
		int kmax = 6;
		int MaxNFE = instance.getNFE();
		int NPMax = 30;
		int NPMin = 6;
		double lambda = 0.5;
		for (int i = 0; i < iteration; i++) {

			SimpleWWOAlgForMLSIAP alg = new SimpleWWOAlgForMLSIAP(instance, false, "SimWWO", MaxNFE, alpha, betaMax,
					betaMin, kmax, lambda, NPMax, NPMin);
			System.out.println(alg.getAlgName() + " 第" + i + "次迭代");
			TwoTuple<WaveForMLSIAP, Double> result = alg.run();
			// 得到记录
			alg.getRecords();

			list.add(result);
		}

		// 计算总时间
		double totalTime = 0.0;
		for (int i = 0; i < iteration; i++) {
			totalTime += list.get(i).second;
		}

		for (TwoTuple<WaveForMLSIAP, Double> tuple : list) {
			System.out.println(tuple);
		}
		System.out.println("总的运行时间为：" + totalTime + "s");
	}

}
