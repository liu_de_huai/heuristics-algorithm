// package test;
//
// import java.util.List;
// import java.util.Random;
//
// import algorithm.WWOForKnapsackProblem;
// import problem.KnapsackProblem;
// import solution.Solution;
//
// public class TestKnapsackAlg {
// @org.junit.Test
// public void test1() {
// Random random=new Random();
// //输入问题所需要的参数
// int dimension=10;
// int[] values=new int[dimension];
// for(int i=0;i<dimension;i++) {
// values[i]=i;
// }
// int[] weights=new int[dimension];
// for(int i=0;i<dimension;i++) {
// weights[i]= random.nextInt(11);
// }
//
// KnapsackProblem problem=new KnapsackProblem(true, "Knapsack problem", 10, 1,
// 0, 50, values, weights);
// WWOForKnapsackProblem alg=new WWOForKnapsackProblem(problem, problem.isMax(),
// "WWO", 20, 1000, 6, 1.001, 1.01, 5, 0.5);
// System.out.println(alg);
// alg.initialize();
// List<Solution<Integer>> population=alg.getPopulation();
// for(Solution<Integer> solution:population) {
// System.out.println(solution);
// }
// }
// }
