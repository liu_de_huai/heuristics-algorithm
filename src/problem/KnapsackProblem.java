//package problem;
//
//import solution.Solution;
//
///**
// * 背包问题
// * 
// * @author Administrator
// *
// */
//public class KnapsackProblem extends CombinatorialProblem<Integer> {
//
//	protected final int capacity;// 背包容量
//
//	protected final int[] values;// 每个物品的价值
//
//	protected final int[] weights;// 每个物品的重量
//
//	public KnapsackProblem(boolean isMax, String problemName, int dimension, Integer upper, Integer lower, int capacity,
//			int[] values, int[] weights) {
//		super(isMax, problemName, dimension, upper, lower);
//		this.capacity = capacity;
//		this.values = values;
//		this.weights = weights;
//	}
//
//	public int getCapacity() {
//		return capacity;
//	}
//
//	public int[] getValues() {
//		return values;
//	}
//
//	public int[] getWeights() {
//		return weights;
//	}
//
//	@Override
//	public boolean judgeConstraint(Solution<Integer> solution) {
//		// 检查是否符合定义域
//		if (solution.check()) {
//			int totalWeight = 0;
//			Integer[] content = solution.getContent();
//			for (int i = 0; i < dimension; i++) {
//				totalWeight += weights[i] * content[i];
//			}
//			// 检查是否符合约束条件
//			if (totalWeight <= capacity) {
//				return true;
//			}
//		}
//		return false;
//	}
//
//	@Override
//	public double calculate(Solution<Integer> solution) {
//		// 如果满足约束条件
//		if (judgeConstraint(solution)) {
//			Integer[] content = solution.getContent();
//			int totalValues = 0;
//			for (int i = 0; i < dimension; i++) {
//				totalValues += values[i] * content[i];
//			}
//			//将计算结果设置到解中
//			solution.setValue(totalValues);
//			return totalValues;
//
//		}
//		// 这里如果不满足约束条件，设置为-1
//		return -1;
//	}
//
//}
