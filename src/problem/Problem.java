package problem;

import solution.Solution;

/**
 * 表示要解决的问题 1.表示问题 2.约束条件，对于不满足约束条件的情况，如何处理？
 * 
 * @param T
 *            指定上下界的表示
 * @param Tuple
 *            指定calculate方法的返回值
 * @param T2
 *            指定Solution方法的第二个泛型数值
 * 
 */
public abstract class Problem<T extends Number, Tuple, T2 extends Number> {

	protected final boolean isMax;// 标识求最大还是最小
	protected final String problemName;// 问题名字
	protected final int dimension;// 问题的维度
	protected final int NFE;// 迭代次数
	protected final T upper;// 上界
	protected final T lower;// 下界

	public Problem(boolean isMax, String problemName, int dimension, T upper, T lower, int NFE) {
		super();
		this.isMax = isMax;
		this.problemName = problemName;
		this.dimension = dimension;
		this.upper = upper;
		this.lower = lower;
		this.NFE = NFE;
	}

	public boolean isMax() {
		return isMax;
	}

	public String getProblemName() {
		return problemName;
	}

	public int getDimension() {
		return dimension;
	}

	public T getUpper() {
		return upper;
	}

	public T getLower() {
		return lower;
	}

	public int getNFE() {
		return NFE;
	}

	/**
	 * 判断是否满足约束条件
	 * 
	 * @param solution
	 * @return
	 */
	public abstract boolean judgeConstraint(Solution<T, T2> solution);

	/**
	 * 对传入的解计算其值
	 * 
	 * @param solution
	 * @return
	 */
	public abstract Tuple calculate(Solution<T, T2> solution);

}
