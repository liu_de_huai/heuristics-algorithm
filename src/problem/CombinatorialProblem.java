package problem;

public abstract class CombinatorialProblem<T extends Number, S, T2 extends Number> extends Problem<T, S, T2> {

	public CombinatorialProblem(boolean isMax, String problemName, int dimension, T upper, T lower, int NFE) {
		super(isMax, problemName, dimension, upper, lower, NFE);
	}

}
