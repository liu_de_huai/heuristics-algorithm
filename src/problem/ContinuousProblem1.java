//package problem;
//
//import solution.Solution;
//import solution.WaveForContinuousProblem1;
//
///**
// * 连续优化问题1
// *
// */
////public class ContinuousProblem1 extends ContinuousProblem<Double> {
////
////	public ContinuousProblem1(boolean isMax, String problemName, int dimension, Double upper, Double lower) {
////		super(isMax, problemName, dimension, upper, lower);
////	}
////
////	/*
////	 * 判断包括定义域在内的约束条件
////	 */
////	@Override
////	public boolean judgeConstraint(Solution<Double> solution) {
////		// 如果在定义域内
////		if (solution.check())
////			return true;
////		return false;
////	}
////
////	@Override
////	public double calculate(Solution<Double> solution) {
////		if (!judgeConstraint(solution)) {
////			System.out.println();
////		}
////		Double[] content=solution.getContent();
////		int n = content.length;
////		double values = 0.0d;
////		for (int i = 1; i <= n; i++) {
////			for (int k = 0; k <= 20; k++) {
////				values += Math.pow(0.5, k) * Math.cos(Math.pow(3, k) * Math.PI * content[i] + 0.5)
////						+ Math.sin(Math.pow(5, k) * Math.PI * content[i]);
////			}
////		}
////
////		return values;
////	}
////
////}
